package gui;

import apptemplate.AppTemplate;
import components.AppWorkspaceComponent;
import controller.HangmanController;
import data.GameData;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import propertymanager.PropertyManager;
import ui.AppGUI;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.awt.*;
import java.io.IOException;
import java.util.Set;

import static hangman.HangmanProperties.*;
import static java.awt.Color.BLACK;

/**
 * This class serves as the GUI component for the Hangman game.
 *
 * @author Ritwik Banerjee
 */
public class Workspace extends AppWorkspaceComponent {

    AppTemplate app; // the actual application
    AppGUI      gui; // the GUI inside which the application sits

    Label             guiHeadingLabel;   // workspace (GUI) heading label
    HBox              headPane;          // conatainer to display the heading
    HBox              bodyPane;          // container for the main game displays
    ToolBar           footToolbar;       // toolbar for game buttons
    BorderPane        figurePane;        // container to display the namesake graphic of the (potentially) hanging person
    VBox              gameTextsPane;     // container to display the text-related parts of the game
    HBox              guessedLetters;    // text area displaying all the letters guessed so far
    HBox              remainingGuessBox; // container to display the number of remaining guesses
    Button            startGame;         // the button to start playing a game of Hangman
    HangmanController controller;
    StackPane         guessedletterPane;
    private Text  words;
    GraphicsContext gc;

    /**
     * Constructor for initializing the workspace, note that this constructor
     * will fully setup the workspace user interface for use.
     *
     * @param initApp The application this workspace is part of.
     * @throws IOException Thrown should there be an error loading application
     *                     data for setting up the user interface.
     */
    public Workspace(AppTemplate initApp) throws IOException {
        app = initApp;
        gui = app.getGUI();
        controller = (HangmanController) gui.getFileController();    //new HangmanController(app, startGame); <-- THIS WAS A MAJOR BUG!??
        layoutGUI();     // initialize all the workspace (GUI) components including the containers and their layout
        setupHandlers(); // ... and set up event handling
    }

    private void layoutGUI() {

        String targetWordColor = "#DD0000";
        String targetBoxColor = "#000000";
        int figurePaneW = 800;
        int figurePaneH = 550;
        PropertyManager propertyManager = PropertyManager.getManager();
        guiHeadingLabel = new Label(propertyManager.getPropertyValue(WORKSPACE_HEADING_LABEL));

        headPane = new HBox();
        headPane.getChildren().add(guiHeadingLabel);
        headPane.setAlignment(Pos.CENTER);

        figurePane = new BorderPane();
        figurePane.setPrefSize(figurePaneW,figurePaneH);
        drawFigurePane();

        guessedLetters = new HBox();
        guessedletterPane = new StackPane();
        guessedLetters.getChildren().add(guessedletterPane);
        guessedLetters.setAlignment(Pos.CENTER);

        remainingGuessBox = new HBox();
        drawRemainGuessBox();

        gameTextsPane = new VBox();
        gc.setFill(Paint.valueOf(targetBoxColor));

        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);

        bodyPane = new HBox();
        bodyPane.getChildren().addAll(figurePane, gameTextsPane);

        startGame = new Button("Start Playing");
        HBox blankBoxLeft  = new HBox();
        HBox blankBoxRight = new HBox();
        HBox.setHgrow(blankBoxLeft, Priority.ALWAYS);
        HBox.setHgrow(blankBoxRight, Priority.ALWAYS);
        footToolbar = new ToolBar(blankBoxLeft, startGame, blankBoxRight);

        workspace = new VBox();
        workspace.getChildren().addAll(headPane, bodyPane, footToolbar);
    }

    public StackPane setGuessedletterPane(StackPane guessedletterPane) {
        return guessedletterPane;
    }

    public StackPane getGuessedletterPane() {

        int l= ((GameData) app.getDataComponent()).getTargetWord().length();

        for (int d = 0; d <= l; d++) {

            Rectangle rect = new Rectangle(20, 20);
            rect.setFill(Paint.valueOf("#C0C0C0"));
            rect.setStroke(Paint.valueOf("#000000"));

            guessedletterPane.getChildren().addAll(rect);
        }
        return guessedletterPane;
    }


    public void drawRemainGuessBox(){
        FlowPane lettersPane = new FlowPane();
        Text a= new Text();
        String s1=" ";

        words = new Text();
        for(int i=0;i<26;i++){

            Rectangle rect = new Rectangle(40, 40);
            rect.setFill(Paint.valueOf("#A9A9A9"));
            rect.setStroke(Paint.valueOf("#FFFFFF"));
            lettersPane.getChildren().addAll(rect);
        }
        for(int j=0; j<26;j++){
            a = new Text(Character.toString((char)(j+'A')));
            lettersPane.getChildren().addAll(a+'A');

        }

    }

    public void drawFigurePane(){
        char c=(char)((GameData)app.getDataComponent()).getRemainingGuesses();
        switch (c){
            case 9:
                Line step1 = new Line(100,450,300,450);
                figurePane.getChildren().addAll(step1);
                break;
            case 8:
                Line step2 = new Line(100,100,100,275);
                figurePane.getChildren().addAll(step2);
                break;
            case 7:
                Line step3 = new Line(100,100,300,100);
                figurePane.getChildren().addAll(step3);
                break;
            case 6:
                Line step4 = new Line(500,100,500,130);
                figurePane.getChildren().addAll(step4);
                break;
            case 5:
                Circle step5 = new Circle(500,160,40);
                step5.setStroke(Color.BLACK);
                step5.setFill(null);
                figurePane.getChildren().addAll(step5);
                break;
            case 4:
                Line step6 = new Line(500,240,465,285);
                figurePane.getChildren().addAll(step6);
                break;
            case 3:
                Line step7 = new Line(500,240,545,285);
                figurePane.getChildren().addAll(step7);
                break;
            case 2:
                Line step8 = new Line(500,240,500,285);
                figurePane.getChildren().addAll(step8);
                break;
            case 1:
                Line step9 = new Line(500,285,470,320);
                figurePane.getChildren().addAll(step9);
                break;
            case 0:
                Line step10 = new Line(500,285,530,320);
                figurePane.getChildren().addAll(step10);
                break;
        }

    }
    private void setupHandlers() {
        startGame.setOnMouseClicked(e -> controller.start());
    }

    /**
     * This function specifies the CSS for all the UI components known at the time the workspace is initially
     * constructed. Components added and/or removed dynamically as the application runs need to be set up separately.
     */
    @Override
    public void initStyle() {
        PropertyManager propertyManager = PropertyManager.getManager();

        gui.getAppPane().setId(propertyManager.getPropertyValue(ROOT_BORDERPANE_ID));
        gui.getToolbarPane().getStyleClass().setAll(propertyManager.getPropertyValue(SEGMENTED_BUTTON_BAR));
        gui.getToolbarPane().setId(propertyManager.getPropertyValue(TOP_TOOLBAR_ID));

        ObservableList<Node> toolbarChildren = gui.getToolbarPane().getChildren();
        toolbarChildren.get(0).getStyleClass().add(propertyManager.getPropertyValue(FIRST_TOOLBAR_BUTTON));
        toolbarChildren.get(toolbarChildren.size() - 1).getStyleClass().add(propertyManager.getPropertyValue(LAST_TOOLBAR_BUTTON));

        workspace.getStyleClass().add(CLASS_BORDERED_PANE);
        guiHeadingLabel.getStyleClass().setAll(propertyManager.getPropertyValue(HEADING_LABEL));

    }

    /** This function reloads the entire workspace */
    @Override
    public void reloadWorkspace() {
        /* does nothing; use reinitialize() instead */
    }

    public VBox getGameTextsPane() {
        return gameTextsPane;
    }

    public HBox getRemainingGuessBox() {
        return remainingGuessBox;
    }

    public Button getStartGame() {
        return startGame;
    }

    public void reinitialize() {
        guessedLetters = new HBox();
        guessedLetters.setStyle("-fx-background-color: transparent;");
        remainingGuessBox = new HBox();
        gameTextsPane = new VBox();
        gameTextsPane.getChildren().setAll(remainingGuessBox, guessedLetters);
        bodyPane.getChildren().setAll(figurePane, gameTextsPane);
    }
}
