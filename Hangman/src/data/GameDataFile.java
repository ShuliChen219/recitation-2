package data;

import com.fasterxml.jackson.core.*;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.cfg.MapperConfig;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.introspect.AnnotatedField;
import com.fasterxml.jackson.databind.introspect.AnnotatedMethod;
import components.AppDataComponent;
import components.AppFileComponent;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.util.Iterator;

/**
 * @author Ritwik Banerjee shuli(kyrio) chen
 */
public class GameDataFile implements AppFileComponent {

    public static final String TARGET_WORD  = "TARGET_WORD";
    public static final String GOOD_GUESSES = "GOOD_GUESSES";
    public static final String BAD_GUESSES  = "BAD_GUESSES";

    @Override
    public void saveData(AppDataComponent data, Path to) throws IOException{
        GameData gamedata = (GameData)data;
        JsonFactory jsonFactory = new JsonFactory();

        try {
            OutputStream e = Files.newOutputStream(to, new OpenOption[0]);
            Throwable var8 = null;

            try {
                JsonGenerator generator = jsonFactory.createGenerator(e, JsonEncoding.UTF8);
                generator.writeStartObject();
                generator.writeStringField("TARGET_WORD", gamedata.getTargetWord());
                generator.writeFieldName("GOOD_GUESSES");
                generator.writeStartArray(gamedata.getGoodGuesses().size());
                Iterator var10 = gamedata.getGoodGuesses().iterator();

                Character c;
                while(var10.hasNext()) {
                    c = (Character)var10.next();
                    generator.writeString(c.toString());
                }

                generator.writeEndArray();
                generator.writeFieldName("BAD_GUESSES");
                generator.writeStartArray(gamedata.getBadGuesses().size());
                var10 = gamedata.getBadGuesses().iterator();

                while(var10.hasNext()) {
                    c = (Character)var10.next();
                    generator.writeString(c.toString());
                }

                generator.writeEndArray();
                generator.writeEndObject();
                generator.close();
            } catch (Throwable var20) {
                var8 = var20;
                throw var20;
            } finally {
                if(e != null) {
                    if(var8 != null) {
                        try {
                            e.close();
                        } catch (Throwable var19) {
                            var8.addSuppressed(var19);
                        }
                    } else {
                        e.close();
                    }
                }

            }
        } catch (IOException var22) {
            var22.printStackTrace();
            System.exit(1);
        }


    }

    @Override
    public void loadData(AppDataComponent data, Path from) throws IOException {
        GameData gamedata = (GameData)data;
        gamedata.reset();
        JsonFactory jsonFactory = new JsonFactory();
        InputStream in = Files.newInputStream(from, new OpenOption[0]);
        Throwable var6 = null;

        try {
            JsonParser jsonParser = jsonFactory.createParser(in);

            label148:
            while(jsonParser.nextToken() != JsonToken.END_OBJECT) {
                String fieldname = jsonParser.getCurrentName();
                int var10 = -1;
                        if(fieldname.equals("BAD_GUESSES")) {
                            var10 = 2;
                        }
                        if(fieldname.equals("GOOD_GUESSES")) {
                            var10 = 1;
                        }
                        if(fieldname.equals("TARGET_WORD")) {
                            var10 = 0;
                        }

                switch(var10) {
                    case 0:
                        gamedata.setTargetWord(jsonParser.nextToken().asString());
                        break;
                    case 1:
                        jsonParser.nextToken();

                        while(true) {
                            if(jsonParser.nextToken() == JsonToken.END_ARRAY) {
                                continue label148;
                            }

                            gamedata.addGoodGuess(jsonParser.getText().charAt(0));
                        }
                    case 2:
                        jsonParser.nextToken();

                        while(true) {
                            if(jsonParser.nextToken() == JsonToken.END_ARRAY) {
                                continue label148;
                            }

                            gamedata.addBadGuess(jsonParser.getText().charAt(0));
                        }
                    default:
                        throw new JsonParseException(jsonParser, "Unable to load JSON data");
                }
            }
        } catch (Throwable var18) {
            var6 = var18;
            throw var18;
        } finally {
            if(in != null) {
                if(var6 != null) {
                    try {
                        in.close();
                    } catch (Throwable var17) {
                        var6.addSuppressed(var17);
                    }
                } else {
                    in.close();
                }
            }

        }
    }

    /** This method will be used if we need to export data into other formats. */
    @Override
    public void exportData(AppDataComponent data, Path filePath) throws IOException { }
}
